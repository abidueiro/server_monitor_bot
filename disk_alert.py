#!/usr/bin/env python3
# coding=utf-8

import subprocess
import requests
import psutil
import time

#Variables
cpu_threshold=80
mem_threshold=75
disk_threshold=75
logfile='/home/alfons/monitor.log'
partition = "/"
hostname = ""
TOKEN = ""
chat_id = ""

def check_once():

    #Disc Alert
 
    df = subprocess.Popen(["df","-h"], stdout=subprocess.PIPE)
    for line in df.stdout:
        splitline = line.decode().split()
        if splitline[5] == partition:
            if int(splitline[4][:-1]) > disk_threshold:
                message = "⚠️ ALERTA disc a " + hostname + " al " + splitline[4][:-1] + "%"
                url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={message}"
                print(requests.get(url).json())
            # else:
            #     message = "disc a " + hostname + " correcte"
            #     url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={message}"
            #     print(requests.get(url).json())
    

    #CPU Alert

    psutil.cpu_count()
    psutil_cpu_current = psutil.cpu_percent(5)
    if psutil_cpu_current > cpu_threshold:
        message = "⚠️ ALERTA la CPU a " + hostname + " al " + str(psutil_cpu_current) + "%"
        url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={message}"
        print(requests.get(url).json())
        # with open(logfile, 'a') as file_handle:
        #     file_handle.write(time.strftime("%c") + ' - CPU ALERT - CPU usage is currently above %s percent' % cpu_threshold)
    else:
        print (time.strftime("%c") + ' - cpu is below %s percent' % cpu_threshold)
        # with open(logfile, 'a') as file_handle:
        #     file_handle.write(time.strftime("%c") + ' - cpu is below %s percent' % cpu_threshold)
    
    #Memory Alert

    if psutil.virtual_memory().percent > mem_threshold:
        message = "⚠️ ALERTA la RAM a " + hostname + " al " + str(psutil.virtual_memory().percent) + "%"
        url = f"https://api.telegram.org/bot{TOKEN}/sendMessage?chat_id={chat_id}&text={message}"
        print(requests.get(url).json())
        # with open(logfile, 'a') as file_handle:
        #     file_handle.write(time.strftime("%c") + ' - MEMORY ALERT - Memory usage is currently above %s percent' % mem_threshold)

check_once()
